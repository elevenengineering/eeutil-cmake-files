#=============================================================================
# Copyright 2017 Eleven Engineering Inc.
#
# Distributed under the OSI-approved BSD License.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#=============================================================================
find_package(PythonInterp 3.4 REQUIRED)
find_package(Git REQUIRED)
set(kPythonExecOrig ${PYTHON_EXECUTABLE})
set(PY_EXEC_PATHS )
set(PY_FILES )


# ------------------------------------------------------------------------------
# gen_git_hashsum_str
# Generates a string based on the commit hashsum and working tree state of a Git
# repository.
# Inputs:
#   src_dir : Path to Git repository
# Outputs:
#   out_str : String with hashsum and dirty state
# ------------------------------------------------------------------------------
function(gen_git_hashsum_str src_dir out_str)
execute_process(
  COMMAND ${GIT_EXECUTABLE} describe --always --dirty
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  OUTPUT_VARIABLE describe_output
)
string(REGEX MATCH "-dirty" dirty_state ${describe_output})
execute_process(
  COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  OUTPUT_VARIABLE hashsum
)
string(REPLACE "\n" "" hashsum ${hashsum})
set(${out_str} ${hashsum}${dirty_state} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# add_to_py_exec_path
# Adds a list of directories as Python import paths, and adds all py files
# within the directories to the global PY_FILES list.
# Inputs:
#   py_dirs : A list of directories to be treated as import paths for Python
# ------------------------------------------------------------------------------
macro(add_to_py_exec_path py_dirs)
  if (NOT ${py_dirs})
  elseif (NOT PY_EXEC_PATHS)
    set(PY_EXEC_PATHS "${${py_dirs}}")
  else()
    set(PY_EXEC_PATHS "${${py_dirs}};${PY_EXEC_PATHS}")
  endif()

  foreach (py_dir ${${py_dirs}})
    file(GLOB_RECURSE pys ${py_dir}/*.py)
    list(APPEND PY_FILES ${pys})
  endforeach()

  if (CMAKE_HOST_WIN32)
    string(REPLACE ";" "\;" PY_EXEC_PATHS "${PY_EXEC_PATHS}")
  else()
    string(REPLACE ";" ":" PY_EXEC_PATHS "${PY_EXEC_PATHS}")
  endif()

  set(PYTHON_EXECUTABLE
    "cmake" "-E" "env" "PYTHONPATH=${PY_EXEC_PATHS}" "${kPythonExecOrig}"
  )
endmacro()


# ------------------------------------------------------------------------------
# set_fast_source
# Marks a C or C++ source file to have more inlining; the resultant machine code
# is faster but larger.
# Inputs:
#   src : source file to compile with inlining disabled
# ------------------------------------------------------------------------------
function(set_fast_source src)
  set_source_files_properties(${src} PROPERTIES
    COMPILE_FLAGS "-mllvm -inline-threshold=10000"
  )
endfunction()


# ------------------------------------------------------------------------------
# swap_suffix
# Common pattern used to change the suffix of a file while maintaining path.
# Inputs:
#   in     : File whose dirname + extensionless name to use
#   suffix : Desired output file's suffix
# Outputs:
#   out    : Desired output file path
# ------------------------------------------------------------------------------
function(swap_suffix in suffix out)
  get_filename_component(input_we ${in} NAME_WE)
  get_filename_component(input_dir ${in} DIRECTORY)
  set(${out} ${input_dir}/${input_we}${suffix} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# gen_dest_vars
# Generates a standardized output directory and target name given an input
# directory.
# Inputs:
#   src_dir   : Firmware source directory
# Outputs:
#   dest_dir  : Firmware destination directory
#   dest_name : Name of the generated executable target for linking
# ------------------------------------------------------------------------------
function(gen_dest_vars src_dir dest_dir dest_name)
  string(REPLACE
    ${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR} local_dest_dir ${src_dir}
  )
  get_filename_component(local_dest_name ${src_dir} NAME)
  file(MAKE_DIRECTORY ${local_dest_dir})
  set(${dest_dir} ${local_dest_dir} PARENT_SCOPE)
  set(${dest_name} ${local_dest_name} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# get_directories
# Returns the list of directories containing all file paths passed in as inputs.
# Outputs:
#   dirs : List of directories
# Inputs:
#   ARGN : Variadic number of input files
# ------------------------------------------------------------------------------
function(get_directories dirs)
  foreach (f ${ARGN})
    get_filename_component(file_dir ${f} DIRECTORY)
    list(APPEND dirs_local ${file_dir})
  endforeach()
  if (dirs_local)
    list(REMOVE_DUPLICATES dirs_local)
  endif()
  set(${dirs} ${dirs_local} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# subdirlist
# Finds all immediate subdirectories of a root directory.
# Inputs:
#   dir     : Root directory
# Outputs:
#   dirlist : List of subdirectories
# ------------------------------------------------------------------------------
function(subdirlist dir dirlist)
  file(GLOB children ${dir}/*)
  foreach (child ${children})
    if (IS_DIRECTORY ${child})
      list(APPEND dirlist_local ${child})
    endif()
  endforeach()
  set(${dirlist} ${dirlist_local} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# get_canonical_gen_output
# Returns the canonical path for the output of a .gen file.
# Inputs:
#   in  : Input .gen file
# Outputs:
#   out : Path to the .gen file's output
# ------------------------------------------------------------------------------
function(get_canonical_gen_output in out)
  string(REPLACE ".gen" "" out_local ${in})
  string(REPLACE
    ${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR}/pycog out_local ${out_local}
  )
  set(${out} ${out_local} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# generate_lists
# Generates header, source, gen, and py lists based on files found within a
# root directory.
# Inputs:
#   dir      : Root directory
# Outputs:
#   srcs     : List of all C and C++ source files found
#   hdrs     : List of all C and C++ headers files found
#   gen_srcs : List of all C and C++ generator sources
#   gen_hdrs : List of all C and C++ generator headers
#   pys      : List of all Python files found
# ------------------------------------------------------------------------------
function(generate_lists dir srcs hdrs gen_srcs gen_hdrs pys)
  file(GLOB_RECURSE srcs_c_local ${dir}/src/*.c)
  file(GLOB_RECURSE srcs_cpp_local ${dir}/src/*.cpp)
  file(GLOB_RECURSE hdrs_c_local ${dir}/inc/*.h)
  file(GLOB_RECURSE hdrs_cpp_local ${dir}/inc/*.hpp)
  file(GLOB_RECURSE gen_srcs_local ${dir}/src/*.c*.gen)
  file(GLOB_RECURSE gen_hdrs_local ${dir}/inc/*.h*.gen)
  file(GLOB_RECURSE pys_local ${dir}/*.py)

  list(APPEND srcs_local ${srcs_c_local} ${srcs_cpp_local})
  list(APPEND hdrs_local ${hdrs_c_local} ${hdrs_cpp_local})

  set(${srcs} ${srcs_local} PARENT_SCOPE)
  set(${hdrs} ${hdrs_local} PARENT_SCOPE)
  set(${gen_srcs} ${gen_srcs_local} PARENT_SCOPE)
  set(${gen_hdrs} ${gen_hdrs_local} PARENT_SCOPE)
  set(${pys} ${pys_local} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# gen_file_target
# Generates a target based on a file. Useful for multithreaded builds and
# parallel file dependencies.
# Inputs:
#   in : File that the target should depend on
# Outputs:
#   target : Generated target name with properties
# ------------------------------------------------------------------------------
function(gen_file_target in target)
  get_filename_component(file_name ${in} NAME_WE)
  get_filename_component(file_ext ${in} EXT)
  string(REPLACE "." "" file_ext ${file_ext})
  set(local_target ${file_name}-${file_ext})
  set(append_index 0)
  while (TARGET ${local_target})
    set(local_target ${file_name}-${file_ext}${append_index})
    math(EXPR append_index "${append_index}+1")
  endwhile()
  add_custom_target(${local_target} DEPENDS ${in})
  set_target_properties(${local_target} PROPERTIES
    OUTPUT_PATH ${in}
    OUTPUT_NAME ${file_name}
  )
set(${target} ${local_target} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# py_cog_gen
# Calls Python cogapp to generate out, given in, passing along the string
# 'defs' defining global strings available to the Python generator code.
# 'defs' MUST be a variable and MUST be set VERBATIM: no escape character
# sequences (they will be passed literally, which generally will be unwanted).
# Inputs:
#   in     : Input generator file
#   out    : Output generated file
#   defs   : A list of cog string_definitions
#   deps   : A list of additional dependencies
# Outputs:
#   target : Target associated with output generatee file
# ------------------------------------------------------------------------------
function(py_cog_gen in out defs deps target)
  add_custom_command(
    OUTPUT ${out}
    COMMAND ${PYTHON_EXECUTABLE} -m cogapp -d ${${defs}} -o ${out} ${in}
    DEPENDS ${in} ${${deps}}
    VERBATIM
  )
  gen_file_target(${out} local_target)
  set(${target} ${local_target} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# py_cog_gen_autodest
# Calls py_cog_gen on in, automatically generating an output destination.
# Inputs:
#   in     : Input generator file
#   defs   : A list of cog string_definitions
#   deps   : A list of additional dependencies
# Outputs:
#   target : Target associated with output generatee file
# ------------------------------------------------------------------------------
function(py_cog_gen_autodest in defs deps target)
  get_canonical_gen_output(${in} out)
  get_filename_component(out_dir ${out} DIRECTORY)
  file(MAKE_DIRECTORY ${out_dir})
  py_cog_gen(${in} ${out} ${defs} ${deps} local_target)
  set(${target} ${local_target} PARENT_SCOPE)
endfunction()


# ------------------------------------------------------------------------------
# build_library
# Compiles all source files (including pycog-generated ones) into a single
# linkable object not intended to be a final target.
# Inputs:
#   lib_name : library target name
#   src_dir  : root directory of library sources/headers
#   defs     : A list of cog string_definitions
#   deps     : A list of additional dependencies, like asm files
# ------------------------------------------------------------------------------
macro(build_library lib_name src_dir defs deps)
  generate_lists(${src_dir} srcs hdrs gen_srcs gen_hdrs pys)
  get_directories(local_py_dirs ${pys})
  add_to_py_exec_path(local_py_dirs)

  set(extra_deps )
  foreach (in_f ${gen_srcs})
    py_cog_gen_autodest(${in_f} ${defs} ${deps} out_target)
    list(APPEND extra_deps ${out_target})
    get_target_property(out_f ${out_target} OUTPUT_PATH)
    list(APPEND srcs ${out_f})
  endforeach()
  foreach (in_f ${gen_hdrs})
    py_cog_gen_autodest(${in_f} ${defs} ${deps} out_target)
    list(APPEND extra_deps ${out_target})
    get_target_property(out_f ${out_target} OUTPUT_PATH)
    list(APPEND hdrs ${out_f})
  endforeach()

  get_directories(hdr_dirs ${hdrs})
  add_library(${lib_name} STATIC ${srcs} ${hdrs})
  if (extra_deps)
    add_dependencies(${lib_name} ${extra_deps})
  endif()
  target_include_directories(${lib_name} BEFORE PUBLIC ${hdr_dirs})
endmacro()


# ------------------------------------------------------------------------------
# build_executable
# Compiles all source files (including pycog-generated ones) into a single
# linkable object intended to be a final target.
# Inputs:
#   exec_name : executable target name
#   src_dir   : root directory of executable sources/headers
#   dest_dir  : directory to build object in
#   defs      : A list of cog string_definitions
#   deps      : A list of additional dependencies, like asm files
# ------------------------------------------------------------------------------
macro(build_executable exec_name src_dir dest_dir defs deps)
  generate_lists(${src_dir} srcs hdrs gen_srcs gen_hdrs pys)
  get_directories(local_py_dirs ${pys})
  add_to_py_exec_path(local_py_dirs)

  set(extra_deps )
  foreach (in_f ${gen_srcs})
    py_cog_gen_autodest(${in_f} ${defs} ${deps} out_target)
    list(APPEND extra_deps ${out_target})
    get_target_property(out_f ${out_target} OUTPUT_PATH)
    list(APPEND srcs ${out_f})
  endforeach()
  foreach (in_f ${gen_hdrs})
    py_cog_gen_autodest(${in_f} ${defs} ${deps} out_target)
    list(APPEND extra_deps ${out_target})
    get_target_property(out_f ${out_target} OUTPUT_PATH)
    list(APPEND hdrs ${out_f})
  endforeach()

  get_directories(hdr_dirs ${hdrs})
  add_executable(${exec_name} ${srcs} ${hdrs})
  if (extra_deps)
    add_dependencies(${exec_name} ${extra_deps})
  endif()
  target_include_directories(${exec_name} BEFORE PUBLIC ${hdr_dirs})
  set_target_properties(${exec_name} PROPERTIES
    RUNTIME_OUTPUT_NAME ${exec_name}
    RUNTIME_OUTPUT_DIRECTORY ${dest_dir}
  )
endmacro()
