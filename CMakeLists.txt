set(PROJECT_NAME EeUtil)
project(${PROJECT_NAME} NONE)
cmake_minimum_required(VERSION 3.6)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})
list(REMOVE_DUPLICATES CMAKE_MODULE_PATH)

include(EeUtilMacros)

set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "Support CMake files for finding Eleven Engineering helper CMake functions."
)
set(CPACK_PACKAGE_VENDOR "Eleven Engineering Inc")
set(CPACK_PACKAGE_CONTACT "edmonton@eleveneng.com")
set(CPACK_PACKAGE_VERSION_MAJOR "1")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "0")

if (WIN32)
  set(cmake_dest ".")
else()
  set(cmake_dest "lib/cmake/${PROJECT_NAME}")
endif()
if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
  set(CPACK_TYPE "DEB")
  set(CPACK_DEBIAN_COMPRESSION_TYPE "xz")
  set(CPACK_DEBIAN_PACKAGE_DEPENDS
    "make(>=0),python3(>=3.4),python3-pip(>=0),cmake(>=3.3.2)"
  )
  set(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA ${PROJECT_SOURCE_DIR}/deb/postinst)
endif()

gen_git_hashsum_str(${PROJECT_SOURCE_DIR} hashsum)
set(in_version_file ${PROJECT_SOURCE_DIR}/${PROJECT_NAME}Config.cmake.in)
set(out_version_file ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake)
configure_file(${in_version_file} ${out_version_file} @ONLY)

install(DIRECTORY DESTINATION ${cmake_dest})
install(
  FILES ${out_version_file} ${PROJECT_SOURCE_DIR}/${PROJECT_NAME}Macros.cmake
  DESTINATION ${cmake_dest}
)

include(CPack)
add_custom_target(pkg
  COMMAND cpack -G "${CPACK_TYPE}"
)
